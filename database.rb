#!/bin/env ruby
# frozen_string_literal: true

require 'sequel'
require 'date'

DB = Sequel.connect 'sqlite://database.db'
DB.create_table? :user do
  primary_key :id
  String :name
  String :email
  String :phone
  Integer :card_number
  TrueClass :active
  TrueClass :proedreio
  TrueClass :asylia
  TrueClass :banned
  Integer :apousies
  Float :balance
  Integer :telegram_id
end

DB.create_table? :settings do
  Integer :max_apousies
  Float :prova_cost
end

DB.create_table? :log_entry do
  primary_key :entry_id
  datetime :timestamp
  foreign_key :id, :user
  Float :amount_paid
  Float :amount_donated
  String :comments
  TrueClass :ban
  TrueClass :apousia
  TrueClass :proedreio
  TrueClass :asylia
end
