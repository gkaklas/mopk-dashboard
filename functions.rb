#!/bin/env ruby

def get_boolean(value)
  if value
    ret = "✅"
  else
    ret = "❌"
  end
  return ret
end
