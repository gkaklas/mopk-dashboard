function setData(selectedObject) {
  var optionData = selectedObject.options[selectedObject.selectedIndex].dataset
  if (selectedObject.value == 0) {
    var balance = ""
    var href = ""
    var proedreio = (optionData.proedreio == 'false');
    var asylia = (optionData.asylia == 'false');
    var banned = (optionData.banned == 'false');
  } else {
    var balance = "€" + optionData.balance;
    var href = "/user?uid=" + selectedObject.value;
    var proedreio = (optionData.proedreio == 'true');
    var asylia = (optionData.asylia == 'true');
    var banned = (optionData.banned == 'true');
  }

  document.getElementById('balance').innerHTML = balance;
  document.getElementById('user-link').href = href;
  document.getElementById('proedreio').checked = proedreio;
  document.getElementById('asylia').checked = asylia;
  document.getElementById('ban').checked = banned;
}
