#!/bin/env ruby
# frozen_string_literal: true

Bundler.require(:default, :development)
require 'slim/include'
require './functions'
use BetterErrors::Middleware

DB = Sequel.connect('sqlite://database.db')

set :port, 8091
set :nav, [
  { title: 'Χρήστες', href: '/users' },
  { title: 'Συνέλευση', href: '/input' },
  { title: 'Ρυθμίσεις', href: '/settings' }
]

get '/users' do
  l = {}
  l[:users] = DB[:user].all
  slim :users, locals: l
end

get '/user' do
  l = {}
  l[:user] = DB[:user].where(id: params['uid']).first
  l[:logs] = DB[:log_entry].where(id: params['uid']).reverse(:timestamp).all
  slim :user, locals: l
end

post '/user' do
  DB[:user].where(id: params['uid']).update(
    name: params['name'],
    email: params['email'],
    phone: params['phone'],
    card_number: params['card-number'],
    apousies: params['apousies'],
    balance: params['balance'],
    telegram_id: params['telegram-id']
  )
  redirect '/user?uid=' + params['uid']
end

get '/' do
  redirect '/input'
end

get '/input' do
  l = {}
  l[:users] = DB[:user].all
  slim :input, locals: l
end

post '/save' do
  DB[:log_entry].insert(
    timestamp: DateTime.now,
    id: params['user'].to_i,
    amount_paid: params['amount_paid'].to_f,
    amount_donated: params['amount_donated'].to_f,
    comments: params['comments'].to_s,
    ban: params['ban'] == 'on',
    apousia: params['apousia'] == 'on',
    asylia: params['asylia'] == 'on',
    proedreio: params['proedreio'] == 'on'
  )
  DB[:user].where(id: params['user']).update(
    # TODO: wtf why integer
    balance: Sequel[:balance] - params[:amount_paid].to_i,
    asylia: params['asylia'] == 'on',
    proedreio: params['proedreio'] == 'on',
    banned: params['ban'] == 'on',
    active: params['ban'] != 'on'
  )
  redirect '/input'
end

get '/settings' do
  l = {}
  l[:user_settings] = DB[:settings].first
  slim :settings, locals: l
end

post '/settings' do
  DB[:settings].update(max_apousies: params['max_apousies'],
                       prova_cost: params['prova_cost'])
  redirect '/settings'
end
